import sys

import PySide
from PySide import QtCore
from PySide.QtGui import (QApplication, QMainWindow, QMessageBox, QTableWidgetItem, QDialog)

from main_GUI import Ui_MainWindow

from communication_with_database import CommunicationHandler

def display_message_box(text):
    temp = QMessageBox()
    temp.setText(text)
    temp.exec_()

class MainWindow(QMainWindow, Ui_MainWindow):
    def __init__(self, parent=None):
        super(MainWindow, self).__init__(parent)
        self.setupUi(self)

        #set up menuBar connections        
        self.actionHelp.triggered.connect(self.display_help)
        self.actionAbout.triggered.connect(self.display_about)

        #set up loginPanel connections
        self.passwordField.returnPressed.connect(self.try_to_login)
        self.loginButton.clicked.connect(self.try_to_login)

        #set up mainPanel connections   
        self.menuUsageButton.clicked.connect(self.go_to_usage)
        self.menuSelectButton.clicked.connect(self.go_to_select)
        self.menuBackupButton.clicked.connect(self.go_to_backup)
        self.menuShortButton.clicked.connect(self.go_to_short)
        self.menuLogOutButton.clicked.connect(self.log_out)
        self.menuExitButton.clicked.connect(self.exit_app)

        #set up selectPanel connections
        self.selectBackToMenu.clicked.connect(self.back_to_menu)
        self.selectButton.clicked.connect(self.select_query)
        self.selectColumnsField.returnPressed.connect(self.select_query)
        self.selectAddButton.clicked.connect(self.go_to_add_panel)
        self.selectRemoveButton.clicked.connect(self.go_to_remove_panel)

        #set up backupPanel connections
        self.backupBackToMenu.clicked.connect(self.back_to_menu)
        self.backupExport.clicked.connect(self.export_database)

        #set up addPanel connections
        self.addBackToSelect.clicked.connect(self.go_to_select)
        self.addTableBox.currentIndexChanged.connect(self.refresh_add_table)
        self.addAcceptChange.clicked.connect(self.add_to_database)

        #set up removePanel connections
        self.removeBackToSelect.clicked.connect(self.go_to_select)
        self.removeButton.clicked.connect(self.remove_from_database)
        self.removeTableBox.currentIndexChanged.connect(self.refresh_remove_table)
  
        #set up shortPanel connections and stuff
        self.set_up_short()
        self.shortBackToMenu.clicked.connect(self.back_to_menu)
        self.shortButton.clicked.connect(self.perform_short_procedure)
        
        #set up usagePanel connections and stuff
        self.usageBackToMenu.clicked.connect(self.back_to_menu)
        self.usageClear.clicked.connect(self.clear_usage_table)
        self.usageAccept.clicked.connect(self.submit_order)


    def log_out(self):
        CommunicationHandler().disconnect()
        self.usernameField.clear()
        self.passwordField.clear()
        self.go_to_login()

    def go_to_login(self):
        self.cardLayout.setCurrentWidget(self.loginPanel)
    def prepare_menu_for_user(self,username):
        #TODO: create some kind of main usage panel, for creating new order, printing receipt and so on...
        #enable everything and then disable what is needed (support for reloging as different user)
        self.selectRemoveButton.setEnabled(True)
        self.selectAddButton.setEnabled(True)

        if 'sprzedawca' in username:
            self.selectRemoveButton.setEnabled(False)
            self.selectAddButton.setEnabled(False)
            self.menuShortButton.setEnabled(False)
            self.menuBackupButton.setEnabled(False)

        elif 'kierownik' in username:
            self.menuShortButton.setEnabled(False)
            self.menuBackupButton.setEnabled(False)
            
        else: #admin or wlasciciel
        #base for implementing different access levels
            pass
    

    def clear_usage_table(self):
        for x in range(0,self.usageMainTable.rowCount()):
            self.usageMainTable.setItem(x,4,QTableWidgetItem(str(0)))

    def go_to_usage(self):
        response = CommunicationHandler().query("SELECT ID,nazwa,ilosc,cena FROM Produkty").fetchall()
        self.usageMainTable.setColumnCount(5)
        self.usageMainTable.setRowCount(len(response))

        self.usageMainTable.setHorizontalHeaderItem(0,QTableWidgetItem("ID"))
        self.usageMainTable.setHorizontalHeaderItem(1,QTableWidgetItem("nazwa"))
        self.usageMainTable.setHorizontalHeaderItem(2,QTableWidgetItem("dostepne"))
        self.usageMainTable.setHorizontalHeaderItem(3,QTableWidgetItem("cena"))
        self.usageMainTable.setHorizontalHeaderItem(4,QTableWidgetItem("dla klienta "))
        for x in range(0,len(response)):
            for y in range(0,len(response[0])):
                temp = QTableWidgetItem(str(response[x][y]))#.setFlags(QtCore.Qt.ItemIsEditable)
                temp.setFlags(QtCore.Qt.ItemIsEditable)
                self.usageMainTable.setItem(x,y,temp)

        self.clear_usage_table()
        
        self.cardLayout.setCurrentWidget(self.usagePanel)

    def set_up_short(self):
        options=['pracownik miesiaca','najwiekszy obrot miesieczny']
        for x in options:
            self.shortBox.addItem(x)
   
    def perform_short_procedure(self):
        # wykonaj procedure i wyswietl wynik na shortTable
        CommunicationHandler().perform_procedure(self.shortBox.currentText())


   

    def import_database(self):
        #mysql -u username -p new_database < data-dump.sql
        pass
    def exit_app(self):
        CommunicationHandler().disconnect()
        sys.exit(0)

    def remove_from_database(self):
        print("REMOVE") 
        column_names=[]
        columns=[]

        for x in range(0,self.removeEntryTable.columnCount()):
            column_names.append(self.removeEntryTable.horizontalHeaderItem(x).text())
            columns.append(self.removeEntryTable.item(self.removeEntryTable.currentRow(),x).text())

        result = CommunicationHandler().remove_from_database(column_names,columns,self.removeTableBox.currentText())
        if result==True:
            self.refresh_remove_table()
            display_message_box("Udane usunięcie")
        else:
            display_message_box("Nie udało się usunąć")
        
        print(columns)
        print(column_names)


    def add_to_database(self):
        columns = []
        column_names = []

        for x in range(0,self.addEntryTable.columnCount()):
            try:
                if self.addEntryTable.item(0,x)==None:
                    display_message_box("Nie wszystkie wartosci zostaly podane!")
                    return
            except NotImplementedError: #If you got an exception, than thab object exists and just can't be compared
                pass
            column_names.append(self.addEntryTable.horizontalHeaderItem(x).text())
            columns.append(self.addEntryTable.item(0,x).text())
       
        print(columns)
        result = CommunicationHandler().insert_to_database(column_names,columns,self.addTableBox.currentText())
        if result==True:
            display_message_box("Dane zostaly pomyslnie dodane")
        else:
            display_message_box("Nie udalo sie dodac danych!")
    
    def submit_order(self):
        ids = []
        count = []

        for x in range(0,self.usageMainTable.rowCount()):
            if self.usageMainTable.item(x,4).text()!=str(0):
                ids.append(self.usageMainTable.item(x,0).text())
                count.append(self.usageMainTable.item(x,4).text())

        print(ids)
        print(count)

        response=CommunicationHandler().submit_order(ids,count)

        if response==True:
            display_message_box("Pomyslnie dodano zamowienie!")
            self.clear_usage_table()
        else:
            display_message_box("Nie udalo sie dokonac zamowienia")

    def refresh_add_table(self):
        self.addEntryTable.setColumnCount(0)
        self.addEntryTable.setRowCount(0)
        self.addEntryTable.setRowCount(1)
        columns = CommunicationHandler().get_all_columns(self.addTableBox.currentText()) 
        self.addEntryTable.setColumnCount(len(columns))
        for x in range(0,len(columns)):
            self.addEntryTable.setHorizontalHeaderItem(x,QTableWidgetItem(str(columns[x][0])))
    
    def refresh_remove_table(self):
        self.removeEntryTable.setColumnCount(0)
        self.removeEntryTable.setRowCount(0)
        self.removeEntryTable.setRowCount(1)
        columns = CommunicationHandler().get_all_columns(self.removeTableBox.currentText()) 
        self.removeEntryTable.setColumnCount(len(columns))
        for x in range(0,len(columns)):
            self.removeEntryTable.setHorizontalHeaderItem(x,QTableWidgetItem(str(columns[x][0])))
        
        response = CommunicationHandler().get_all_fields(self.removeTableBox.currentText())

        for x in range(0,len(columns)):
                self.removeEntryTable.setHorizontalHeaderItem(x,QTableWidgetItem(str(columns[x][0])))

        rows = len(response)
        if len(response)>0:
            self.removeEntryTable.setColumnCount(len(response[0]))
            self.removeEntryTable.setRowCount(rows)
            for x in range(0,len(response)):
                for y in range(0,len(response[0])):
                    self.removeEntryTable.setItem(x,y,QTableWidgetItem(str(response[x][y])))
        else:
            display_message_box("Tabela jest pusta!")

   

    def export_database(self):
        CommunicationHandler().query('\! mysqldump -u root 3_list > database_backup.sql')

    def go_to_backup(self):
        self.cardLayout.setCurrentWidget(self.backupPanel)

    def go_to_select(self):
        tables = CommunicationHandler().get_all_tables()
        self.selectTableBox.clear()
        self.selectTableBox.setMaxCount(len(tables))
        self.selectTable.setColumnCount(0)
        self.selectTable.setRowCount(0)
        self.selectColumnsField.clear()
        for x in tables:
            self.selectTableBox.addItem(x[0])
        
        self.cardLayout.setCurrentWidget(self.selectPanel)

    def go_to_remove_panel(self):
        tables = CommunicationHandler().get_all_tables()
        self.removeTableBox.clear()
        self.removeTableBox.setMaxCount(len(tables))
        self.removeEntryTable.setColumnCount(0)
        self.removeEntryTable.setRowCount(0)
        self.removeEntryTable.setRowCount(0)
        for x in tables:
            self.removeTableBox.addItem(x[0])
        self.refresh_remove_table()
        
        self.cardLayout.setCurrentWidget(self.removePanel)

    def go_to_add_panel(self):
        tables = CommunicationHandler().get_all_tables()
        self.addTableBox.clear()
        self.addTableBox.setMaxCount(len(tables))
        self.addEntryTable.setColumnCount(0)
        self.addEntryTable.setRowCount(0)
        self.addEntryTable.setRowCount(0)
        for x in tables:
            self.addTableBox.addItem(x[0])
        self.refresh_add_table()
        
        self.cardLayout.setCurrentWidget(self.addPanel)

    def go_to_short(self):
        self.cardLayout.setCurrentWidget(self.shortPanel)

    def back_to_menu(self):
        self.cardLayout.setCurrentWidget(self.mainPanel)

    def display_help(self):
        display_message_box("HELP\n If you need help, ask program creator")

    def display_about(self):
        display_message_box("ABOUT\n by d0ku")

    def try_to_login(self):
        username = self.usernameField.text()
        password = self.passwordField.text()
        #if you want to connect with different host, than add it as a parameter
        connect=CommunicationHandler().connect(username,password)
        message_to_display="Połączono! "
        if connect!="success":
            #set string depending on error code TODO: handle all connection error codes?
            message_to_display = "Nie udało się połączyć: "

            if connect=="2003":
                message_to_display+="skontaktuj się z administratorem"
 
        display_message_box(message_to_display)
        
        if connect=="success":
            self.prepare_menu_for_user(username)
            self.back_to_menu()
            
    
    def select_query(self):
        if len(self.selectColumnsField.text())==0:
            display_message_box("Należy podać parametry")
            return

        columns_read = self.selectColumnsField.text()
        columns_read = columns_read.split(";")

        columns =[]
        for x in columns_read:
            if x !="":
                columns.append(x)
        print(columns)

        
        table = self.selectTableBox.currentText()
        print(table)

        response=CommunicationHandler().select(columns,table)
        if response=="incorrect data":
            display_message_box("Wprowadź poprawne nazwy kolumn")
            self.selectTable.setColumnCount(0)
            self.selectTable.setRowCount(0)
            return
        else:
            rows = len(response)
            print(columns)
            
            if len(response)>0:
                self.selectTable.setColumnCount(len(response[0]))
                self.selectTable.setRowCount(rows)
       
                for x in range(0,len(columns)):
                    self.selectTable.setHorizontalHeaderItem(x,QTableWidgetItem(str(columns[x])))


                for x in range(0,len(response)):
                    for y in range(0,len(response[0])):
                        self.selectTable.setItem(x,y,QTableWidgetItem(str(response[x][y])))
            else: 
                display_message_box("Tabela jest pusta!")

if __name__=='__main__':
    app = QApplication(sys.argv)
    frame = MainWindow()

    frame.show()
    sys.exit(app.exec_())
