# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'main_GUI.ui'
#
# Created: Mon Jan 15 16:45:28 2018
#      by: pyside-uic 0.2.15 running on PySide 1.2.4
#
# WARNING! All changes made in this file will be lost!

from PySide import QtCore, QtGui

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(579, 609)
        self.centralwidget = QtGui.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.horizontalLayout = QtGui.QHBoxLayout(self.centralwidget)
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.cardLayout = QtGui.QStackedWidget(self.centralwidget)
        self.cardLayout.setObjectName("cardLayout")
        self.loginPanel = QtGui.QWidget()
        self.loginPanel.setObjectName("loginPanel")
        self.formLayout = QtGui.QFormLayout(self.loginPanel)
        self.formLayout.setObjectName("formLayout")
        self.gridLayout_3 = QtGui.QGridLayout()
        self.gridLayout_3.setObjectName("gridLayout_3")
        self.label_3 = QtGui.QLabel(self.loginPanel)
        self.label_3.setObjectName("label_3")
        self.gridLayout_3.addWidget(self.label_3, 0, 0, 1, 1)
        self.usernameField = QtGui.QLineEdit(self.loginPanel)
        self.usernameField.setText("")
        self.usernameField.setObjectName("usernameField")
        self.gridLayout_3.addWidget(self.usernameField, 0, 1, 1, 1)
        self.label_4 = QtGui.QLabel(self.loginPanel)
        self.label_4.setObjectName("label_4")
        self.gridLayout_3.addWidget(self.label_4, 1, 0, 1, 1)
        self.passwordField = QtGui.QLineEdit(self.loginPanel)
        self.passwordField.setText("")
        self.passwordField.setEchoMode(QtGui.QLineEdit.Password)
        self.passwordField.setObjectName("passwordField")
        self.gridLayout_3.addWidget(self.passwordField, 1, 1, 1, 1)
        self.loginButton = QtGui.QPushButton(self.loginPanel)
        self.loginButton.setObjectName("loginButton")
        self.gridLayout_3.addWidget(self.loginButton, 1, 2, 1, 1)
        self.formLayout.setLayout(0, QtGui.QFormLayout.FieldRole, self.gridLayout_3)
        self.cardLayout.addWidget(self.loginPanel)
        self.usagePanel = QtGui.QWidget()
        self.usagePanel.setObjectName("usagePanel")
        self.horizontalLayout_3 = QtGui.QHBoxLayout(self.usagePanel)
        self.horizontalLayout_3.setObjectName("horizontalLayout_3")
        self.usageMainTable = QtGui.QTableWidget(self.usagePanel)
        self.usageMainTable.setObjectName("usageMainTable")
        self.usageMainTable.setColumnCount(0)
        self.usageMainTable.setRowCount(0)
        self.horizontalLayout_3.addWidget(self.usageMainTable)
        self.verticalLayout_2 = QtGui.QVBoxLayout()
        self.verticalLayout_2.setObjectName("verticalLayout_2")
        self.usageBackToMenu = QtGui.QPushButton(self.usagePanel)
        self.usageBackToMenu.setObjectName("usageBackToMenu")
        self.verticalLayout_2.addWidget(self.usageBackToMenu)
        self.usageClear = QtGui.QPushButton(self.usagePanel)
        self.usageClear.setObjectName("usageClear")
        self.verticalLayout_2.addWidget(self.usageClear)
        self.usageAccept = QtGui.QPushButton(self.usagePanel)
        self.usageAccept.setObjectName("usageAccept")
        self.verticalLayout_2.addWidget(self.usageAccept)
        self.horizontalLayout_3.addLayout(self.verticalLayout_2)
        self.cardLayout.addWidget(self.usagePanel)
        self.mainPanel = QtGui.QWidget()
        self.mainPanel.setObjectName("mainPanel")
        self.horizontalLayout_4 = QtGui.QHBoxLayout(self.mainPanel)
        self.horizontalLayout_4.setObjectName("horizontalLayout_4")
        self.gridLayout_10 = QtGui.QGridLayout()
        self.gridLayout_10.setObjectName("gridLayout_10")
        self.menuUsageButton = QtGui.QPushButton(self.mainPanel)
        self.menuUsageButton.setObjectName("menuUsageButton")
        self.gridLayout_10.addWidget(self.menuUsageButton, 0, 0, 1, 1)
        self.menuSelectButton = QtGui.QPushButton(self.mainPanel)
        self.menuSelectButton.setObjectName("menuSelectButton")
        self.gridLayout_10.addWidget(self.menuSelectButton, 1, 0, 1, 1)
        self.menuShortButton = QtGui.QPushButton(self.mainPanel)
        self.menuShortButton.setCheckable(False)
        self.menuShortButton.setObjectName("menuShortButton")
        self.gridLayout_10.addWidget(self.menuShortButton, 2, 0, 1, 1)
        self.menuBackupButton = QtGui.QPushButton(self.mainPanel)
        self.menuBackupButton.setObjectName("menuBackupButton")
        self.gridLayout_10.addWidget(self.menuBackupButton, 3, 0, 1, 1)
        self.menuLogOutButton = QtGui.QPushButton(self.mainPanel)
        self.menuLogOutButton.setObjectName("menuLogOutButton")
        self.gridLayout_10.addWidget(self.menuLogOutButton, 4, 0, 1, 1)
        self.menuExitButton = QtGui.QPushButton(self.mainPanel)
        self.menuExitButton.setObjectName("menuExitButton")
        self.gridLayout_10.addWidget(self.menuExitButton, 5, 0, 1, 1)
        self.horizontalLayout_4.addLayout(self.gridLayout_10)
        self.cardLayout.addWidget(self.mainPanel)
        self.selectPanel = QtGui.QWidget()
        self.selectPanel.setObjectName("selectPanel")
        self.gridLayout = QtGui.QGridLayout(self.selectPanel)
        self.gridLayout.setObjectName("gridLayout")
        self.label_5 = QtGui.QLabel(self.selectPanel)
        self.label_5.setObjectName("label_5")
        self.gridLayout.addWidget(self.label_5, 0, 0, 1, 2)
        self.gridLayout_2 = QtGui.QGridLayout()
        self.gridLayout_2.setObjectName("gridLayout_2")
        self.label_6 = QtGui.QLabel(self.selectPanel)
        self.label_6.setObjectName("label_6")
        self.gridLayout_2.addWidget(self.label_6, 0, 0, 1, 2)
        self.selectColumnsField = QtGui.QLineEdit(self.selectPanel)
        self.selectColumnsField.setObjectName("selectColumnsField")
        self.gridLayout_2.addWidget(self.selectColumnsField, 0, 2, 1, 1)
        self.label_7 = QtGui.QLabel(self.selectPanel)
        self.label_7.setObjectName("label_7")
        self.gridLayout_2.addWidget(self.label_7, 1, 0, 1, 2)
        self.selectTableBox = QtGui.QComboBox(self.selectPanel)
        self.selectTableBox.setObjectName("selectTableBox")
        self.gridLayout_2.addWidget(self.selectTableBox, 1, 2, 1, 1)
        self.gridLayout.addLayout(self.gridLayout_2, 1, 0, 1, 1)
        self.horizontalLayout_2 = QtGui.QHBoxLayout()
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        self.selectButton = QtGui.QPushButton(self.selectPanel)
        self.selectButton.setObjectName("selectButton")
        self.horizontalLayout_2.addWidget(self.selectButton)
        self.selectAddButton = QtGui.QPushButton(self.selectPanel)
        self.selectAddButton.setObjectName("selectAddButton")
        self.horizontalLayout_2.addWidget(self.selectAddButton)
        self.selectRemoveButton = QtGui.QPushButton(self.selectPanel)
        self.selectRemoveButton.setObjectName("selectRemoveButton")
        self.horizontalLayout_2.addWidget(self.selectRemoveButton)
        self.gridLayout.addLayout(self.horizontalLayout_2, 1, 1, 1, 1)
        self.selectBackToMenu = QtGui.QPushButton(self.selectPanel)
        self.selectBackToMenu.setObjectName("selectBackToMenu")
        self.gridLayout.addWidget(self.selectBackToMenu, 2, 1, 1, 1)
        self.selectTable = QtGui.QTableWidget(self.selectPanel)
        self.selectTable.setEditTriggers(QtGui.QAbstractItemView.NoEditTriggers)
        self.selectTable.setObjectName("selectTable")
        self.selectTable.setColumnCount(0)
        self.selectTable.setRowCount(0)
        self.gridLayout.addWidget(self.selectTable, 4, 0, 1, 2)
        self.cardLayout.addWidget(self.selectPanel)
        self.backupPanel = QtGui.QWidget()
        self.backupPanel.setObjectName("backupPanel")
        self.gridLayout_4 = QtGui.QGridLayout(self.backupPanel)
        self.gridLayout_4.setObjectName("gridLayout_4")
        self.backupExport = QtGui.QPushButton(self.backupPanel)
        self.backupExport.setObjectName("backupExport")
        self.gridLayout_4.addWidget(self.backupExport, 0, 0, 1, 1)
        self.backupImport = QtGui.QPushButton(self.backupPanel)
        self.backupImport.setObjectName("backupImport")
        self.gridLayout_4.addWidget(self.backupImport, 0, 1, 1, 1)
        self.backupBackToMenu = QtGui.QPushButton(self.backupPanel)
        self.backupBackToMenu.setObjectName("backupBackToMenu")
        self.gridLayout_4.addWidget(self.backupBackToMenu, 1, 2, 1, 1)
        self.cardLayout.addWidget(self.backupPanel)
        self.addPanel = QtGui.QWidget()
        self.addPanel.setObjectName("addPanel")
        self.gridLayout_5 = QtGui.QGridLayout(self.addPanel)
        self.gridLayout_5.setObjectName("gridLayout_5")
        self.label_8 = QtGui.QLabel(self.addPanel)
        self.label_8.setObjectName("label_8")
        self.gridLayout_5.addWidget(self.label_8, 0, 0, 1, 1)
        self.addEntryTable = QtGui.QTableWidget(self.addPanel)
        self.addEntryTable.setRowCount(2)
        self.addEntryTable.setObjectName("addEntryTable")
        self.addEntryTable.setColumnCount(0)
        self.addEntryTable.setRowCount(2)
        self.gridLayout_5.addWidget(self.addEntryTable, 1, 0, 1, 2)
        self.addBackToSelect = QtGui.QPushButton(self.addPanel)
        self.addBackToSelect.setObjectName("addBackToSelect")
        self.gridLayout_5.addWidget(self.addBackToSelect, 2, 1, 1, 2)
        self.addAcceptChange = QtGui.QPushButton(self.addPanel)
        self.addAcceptChange.setObjectName("addAcceptChange")
        self.gridLayout_5.addWidget(self.addAcceptChange, 1, 2, 1, 1)
        self.addTableBox = QtGui.QComboBox(self.addPanel)
        self.addTableBox.setObjectName("addTableBox")
        self.gridLayout_5.addWidget(self.addTableBox, 0, 2, 1, 1)
        self.label_9 = QtGui.QLabel(self.addPanel)
        self.label_9.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.label_9.setObjectName("label_9")
        self.gridLayout_5.addWidget(self.label_9, 0, 1, 1, 1)
        self.cardLayout.addWidget(self.addPanel)
        self.removePanel = QtGui.QWidget()
        self.removePanel.setObjectName("removePanel")
        self.gridLayout_7 = QtGui.QGridLayout(self.removePanel)
        self.gridLayout_7.setObjectName("gridLayout_7")
        self.gridLayout_6 = QtGui.QGridLayout()
        self.gridLayout_6.setObjectName("gridLayout_6")
        self.removeButton = QtGui.QPushButton(self.removePanel)
        self.removeButton.setObjectName("removeButton")
        self.gridLayout_6.addWidget(self.removeButton, 1, 2, 1, 1)
        self.removeBackToSelect = QtGui.QPushButton(self.removePanel)
        self.removeBackToSelect.setObjectName("removeBackToSelect")
        self.gridLayout_6.addWidget(self.removeBackToSelect, 2, 2, 1, 1)
        self.removeTableBox = QtGui.QComboBox(self.removePanel)
        self.removeTableBox.setEditable(False)
        self.removeTableBox.setObjectName("removeTableBox")
        self.gridLayout_6.addWidget(self.removeTableBox, 0, 2, 1, 1)
        self.removeEntryTable = QtGui.QTableWidget(self.removePanel)
        self.removeEntryTable.setEditTriggers(QtGui.QAbstractItemView.AnyKeyPressed|QtGui.QAbstractItemView.DoubleClicked)
        self.removeEntryTable.setObjectName("removeEntryTable")
        self.removeEntryTable.setColumnCount(0)
        self.removeEntryTable.setRowCount(0)
        self.gridLayout_6.addWidget(self.removeEntryTable, 1, 0, 1, 1)
        self.label_10 = QtGui.QLabel(self.removePanel)
        self.label_10.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.label_10.setObjectName("label_10")
        self.gridLayout_6.addWidget(self.label_10, 0, 0, 1, 1)
        self.gridLayout_7.addLayout(self.gridLayout_6, 0, 0, 1, 1)
        self.cardLayout.addWidget(self.removePanel)
        self.shortPanel = QtGui.QWidget()
        self.shortPanel.setObjectName("shortPanel")
        self.gridLayout_9 = QtGui.QGridLayout(self.shortPanel)
        self.gridLayout_9.setObjectName("gridLayout_9")
        self.gridLayout_8 = QtGui.QGridLayout()
        self.gridLayout_8.setObjectName("gridLayout_8")
        self.shortButton = QtGui.QPushButton(self.shortPanel)
        self.shortButton.setObjectName("shortButton")
        self.gridLayout_8.addWidget(self.shortButton, 0, 3, 1, 1)
        self.shortTable = QtGui.QTableWidget(self.shortPanel)
        self.shortTable.setObjectName("shortTable")
        self.shortTable.setColumnCount(0)
        self.shortTable.setRowCount(0)
        self.gridLayout_8.addWidget(self.shortTable, 0, 0, 1, 1)
        self.shortBackToMenu = QtGui.QPushButton(self.shortPanel)
        self.shortBackToMenu.setObjectName("shortBackToMenu")
        self.gridLayout_8.addWidget(self.shortBackToMenu, 1, 3, 1, 1)
        self.shortBox = QtGui.QComboBox(self.shortPanel)
        self.shortBox.setObjectName("shortBox")
        self.gridLayout_8.addWidget(self.shortBox, 0, 1, 1, 1)
        self.gridLayout_9.addLayout(self.gridLayout_8, 0, 0, 1, 1)
        self.cardLayout.addWidget(self.shortPanel)
        self.horizontalLayout.addWidget(self.cardLayout)
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtGui.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 579, 20))
        self.menubar.setObjectName("menubar")
        self.menuHelp = QtGui.QMenu(self.menubar)
        self.menuHelp.setObjectName("menuHelp")
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtGui.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)
        self.toolBar = QtGui.QToolBar(MainWindow)
        self.toolBar.setObjectName("toolBar")
        MainWindow.addToolBar(QtCore.Qt.TopToolBarArea, self.toolBar)
        self.actionHelp = QtGui.QAction(MainWindow)
        self.actionHelp.setObjectName("actionHelp")
        self.actionAbout = QtGui.QAction(MainWindow)
        self.actionAbout.setObjectName("actionAbout")
        self.menuHelp.addAction(self.actionHelp)
        self.menuHelp.addAction(self.actionAbout)
        self.menubar.addAction(self.menuHelp.menuAction())

        self.retranslateUi(MainWindow)
        self.cardLayout.setCurrentIndex(0)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle(QtGui.QApplication.translate("MainWindow", "MainWindow", None, QtGui.QApplication.UnicodeUTF8))
        self.label_3.setText(QtGui.QApplication.translate("MainWindow", "Użytkownik", None, QtGui.QApplication.UnicodeUTF8))
        self.label_4.setText(QtGui.QApplication.translate("MainWindow", "Hasło", None, QtGui.QApplication.UnicodeUTF8))
        self.loginButton.setText(QtGui.QApplication.translate("MainWindow", "Zaloguj", None, QtGui.QApplication.UnicodeUTF8))
        self.usageBackToMenu.setText(QtGui.QApplication.translate("MainWindow", "Wróć", None, QtGui.QApplication.UnicodeUTF8))
        self.usageClear.setText(QtGui.QApplication.translate("MainWindow", "Wyczyść", None, QtGui.QApplication.UnicodeUTF8))
        self.usageAccept.setText(QtGui.QApplication.translate("MainWindow", "Akceptuj", None, QtGui.QApplication.UnicodeUTF8))
        self.menuUsageButton.setText(QtGui.QApplication.translate("MainWindow", "Sprzedaż", None, QtGui.QApplication.UnicodeUTF8))
        self.menuSelectButton.setText(QtGui.QApplication.translate("MainWindow", "Wyświetl, Dodaj, Usuń", None, QtGui.QApplication.UnicodeUTF8))
        self.menuShortButton.setText(QtGui.QApplication.translate("MainWindow", "Procedury", None, QtGui.QApplication.UnicodeUTF8))
        self.menuBackupButton.setText(QtGui.QApplication.translate("MainWindow", "Import/Export", None, QtGui.QApplication.UnicodeUTF8))
        self.menuLogOutButton.setText(QtGui.QApplication.translate("MainWindow", "Wyloguj", None, QtGui.QApplication.UnicodeUTF8))
        self.menuExitButton.setText(QtGui.QApplication.translate("MainWindow", "Wyjdź", None, QtGui.QApplication.UnicodeUTF8))
        self.label_5.setText(QtGui.QApplication.translate("MainWindow", "Wpisz poniżej wartości które chcesz wyświetlić (oddzielone średnikami), oraz nazwę tabeli", None, QtGui.QApplication.UnicodeUTF8))
        self.label_6.setText(QtGui.QApplication.translate("MainWindow", "Wartości:", None, QtGui.QApplication.UnicodeUTF8))
        self.label_7.setText(QtGui.QApplication.translate("MainWindow", "Tabela:", None, QtGui.QApplication.UnicodeUTF8))
        self.selectButton.setText(QtGui.QApplication.translate("MainWindow", "Wyświetl", None, QtGui.QApplication.UnicodeUTF8))
        self.selectAddButton.setText(QtGui.QApplication.translate("MainWindow", "Dodaj", None, QtGui.QApplication.UnicodeUTF8))
        self.selectRemoveButton.setText(QtGui.QApplication.translate("MainWindow", "Usuń", None, QtGui.QApplication.UnicodeUTF8))
        self.selectBackToMenu.setText(QtGui.QApplication.translate("MainWindow", "Wróć", None, QtGui.QApplication.UnicodeUTF8))
        self.backupExport.setText(QtGui.QApplication.translate("MainWindow", "EXPORT", None, QtGui.QApplication.UnicodeUTF8))
        self.backupImport.setText(QtGui.QApplication.translate("MainWindow", "IMPORT", None, QtGui.QApplication.UnicodeUTF8))
        self.backupBackToMenu.setText(QtGui.QApplication.translate("MainWindow", "Wstecz", None, QtGui.QApplication.UnicodeUTF8))
        self.label_8.setText(QtGui.QApplication.translate("MainWindow", "(jeżeli nie znasz zawartości, wpisz X)", None, QtGui.QApplication.UnicodeUTF8))
        self.addBackToSelect.setText(QtGui.QApplication.translate("MainWindow", "Wstecz", None, QtGui.QApplication.UnicodeUTF8))
        self.addAcceptChange.setText(QtGui.QApplication.translate("MainWindow", "Dodaj", None, QtGui.QApplication.UnicodeUTF8))
        self.label_9.setText(QtGui.QApplication.translate("MainWindow", "Tabela:", None, QtGui.QApplication.UnicodeUTF8))
        self.removeButton.setText(QtGui.QApplication.translate("MainWindow", "Usuń", None, QtGui.QApplication.UnicodeUTF8))
        self.removeBackToSelect.setText(QtGui.QApplication.translate("MainWindow", "Wstecz", None, QtGui.QApplication.UnicodeUTF8))
        self.label_10.setText(QtGui.QApplication.translate("MainWindow", "Tabela", None, QtGui.QApplication.UnicodeUTF8))
        self.shortButton.setText(QtGui.QApplication.translate("MainWindow", "Wykonaj", None, QtGui.QApplication.UnicodeUTF8))
        self.shortBackToMenu.setText(QtGui.QApplication.translate("MainWindow", "Wstecz", None, QtGui.QApplication.UnicodeUTF8))
        self.menuHelp.setTitle(QtGui.QApplication.translate("MainWindow", "Pomoc", None, QtGui.QApplication.UnicodeUTF8))
        self.toolBar.setWindowTitle(QtGui.QApplication.translate("MainWindow", "toolBar", None, QtGui.QApplication.UnicodeUTF8))
        self.actionHelp.setText(QtGui.QApplication.translate("MainWindow", "Pomoc", None, QtGui.QApplication.UnicodeUTF8))
        self.actionHelp.setShortcut(QtGui.QApplication.translate("MainWindow", "Ctrl+H", None, QtGui.QApplication.UnicodeUTF8))
        self.actionAbout.setText(QtGui.QApplication.translate("MainWindow", "O programie", None, QtGui.QApplication.UnicodeUTF8))

