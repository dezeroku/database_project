DROP DATABASE flower_power;
CREATE DATABASE flower_power;
-- write some logs
USE flower_power;

CREATE TABLE Produkty
(
    ID int NOT NULL AUTO_INCREMENT, -- manual insert is forbidden
    rodzina VARCHAR(30),
    rzad VARCHAR(30),
    nazwa VARCHAR(30) NOT NULL,
    ilosc INT NOT NULL CHECK (ilosc >=0),
    cena FLOAT(6,2) NOT NULL CHECK (cena >0),
    -- PRIMARY KEY(rodzina, rzad, nazwa)
    PRIMARY KEY(ID),
    UNIQUE(rodzina,rzad,nazwa)
);

-- triggers not yet created, possibly not even needed?
CREATE TABLE Filia
(
    ID int NOT NULL AUTO_INCREMENT, -- manual insert is forbidden
    kraj VARCHAR(30) NOT NULL,
    miasto VARCHAR(30) NOT NULL,
    ulica VARCHAR(30) NOT NULL,
    lokal VARCHAR(30) NOT NULL,
    PRIMARY KEY(ID),
    UNIQUE(kraj,miasto,ulica,lokal)
);

CREATE TABLE Telefon
(
    numer int NOT NULL,
    ID_filia int NOT NULL,
    PRIMARY KEY(numer),
    FOREIGN KEY(ID_filia) REFERENCES Filia(ID)
);

CREATE TABLE Pracownicy
(
    ID int NOT NULL AUTO_INCREMENT, -- manual insert is forbidden
    PESEL CHAR(11) NOT NULL, -- validate PESEL on insert
    data_zatrudnienia DATE NOT NULL, -- data dodania?
    imie VARCHAR(30) NOT NULL,
    ID_filia int NOT NULL,
    nazwisko VARCHAR(30) NOT NULL,
    pensja FLOAT(8,2) NOT NULL CHECK (pensja>0),
    funkcja VARCHAR(30) NOT NULL, -- this should be an enum
    PRIMARY KEY(ID),
    FOREIGN KEY(ID_filia) REFERENCES Filia(ID),
    UNIQUE(PESEL)
);

CREATE TABLE Zawartosc
(
    ID int NOT NULL AUTO_INCREMENT, -- manual insert is forbidden
    ID_zamowienia int NOT NULL,
    ID_produkt int NOT NULL,
    ilosc int NOT NULL,
    cena_kupna FLOAT(6,2), -- is this necessary?
    PRIMARY KEY(ID),
    FOREIGN KEY (ID_produkt) REFERENCES Produkty(ID)
);

CREATE TABLE Zamowienia
(
    ID int NOT NULL AUTO_INCREMENT, -- manual insert is forbidden
    ID_sprzedawcy int NOT NULL,
    ID_filia int NOT NULL,
    potwierdzenie BOOLEAN NOT NULL,
    data_sprzedazy DATETIME NOT NULL,
    platnosc_karta BOOLEAN NOT NULL, -- moze ENUM? blik karta gotowka itd...
    PRIMARY KEY(ID),
    FOREIGN KEY(ID_sprzedawcy) REFERENCES Pracownicy(ID),
    FOREIGN KEY(ID_filia) REFERENCES Filia(ID)
);

