DELIMITER $$

CREATE OR REPLACE TRIGGER check_insert_Produkty BEFORE INSERT ON Produkty
    FOR EACH ROW
	BEGIN
	    IF new.ilosc < 0 THEN
		SIGNAL SQLSTATE '45000'
		SET MESSAGE_TEXT= 'Ilosc nie może być ujemna';
	    END IF;

	    IF new.cena <=0 THEN
		SIGNAL SQLSTATE '45000'
		SET MESSAGE_TEXT ='Cena nie może być ujemna lub równa zero';
	    END IF;
	END;
	$$
DELIMITER ;





DELIMITER $$

CREATE OR REPLACE TRIGGER check_update_Produkty BEFORE UPDATE ON Produkty
    FOR EACH ROW
	BEGIN
	    IF new.ilosc < 0 THEN
		SIGNAL SQLSTATE '45000'
		SET MESSAGE_TEXT= 'Ilosc nie może być ujemna';
	    END IF;

	    IF new.cena <=0 THEN
		SIGNAL SQLSTATE '45000'
		SET MESSAGE_TEXT ='Cena nie może być ujemna lub równa zero';
	    END IF;
	END;
	$$
DELIMITER ;






DELIMITER $$

CREATE OR REPLACE TRIGGER check_insert_Pracownicy BEFORE INSERT ON Pracownicy
    FOR EACH ROW
	BEGIN -- validate PESEL
	    IF new.pensja <= 0 THEN
		SIGNAL SQLSTATE '45000'
		SET MESSAGE_TEXT= 'Pensja nie może być ujemna';
	    END IF;
	END;
	$$
DELIMITER ;





DELIMITER $$

CREATE OR REPLACE TRIGGER check_update_Pracownicy BEFORE UPDATE ON Pracownicy
    FOR EACH ROW
	BEGIN
	    IF new.pensja <= 0 THEN
		SIGNAL SQLSTATE '45000'
		SET MESSAGE_TEXT= 'Pensja nie może być ujemna';
	    END IF;
	END;
	$$
DELIMITER ;









DELIMITER $$

CREATE OR REPLACE TRIGGER check_insert_Zawartosc BEFORE INSERT ON Zawartosc
    FOR EACH ROW
	BEGIN -- validate PESEL
	    IF new.cena_kupna < 0 THEN
		SIGNAL SQLSTATE '45000'
		SET MESSAGE_TEXT= 'Cena nie może być mniejsza od zera';
	    END IF;
	END;
	$$
DELIMITER ;






DELIMITER $$

CREATE OR REPLACE TRIGGER check_update_Zawartosc BEFORE UPDATE ON Zawartosc
    FOR EACH ROW
	BEGIN -- validate PESEL
	    IF new.cena_kupna < 0 THEN
		SIGNAL SQLSTATE '45000'
		SET MESSAGE_TEXT= 'Cena nie może być mniejsza od zera';
	    END IF;
	END;
	$$
DELIMITER ;






DELIMITER $$

CREATE OR REPLACE TRIGGER check_insert_Zamowienia BEFORE INSERT ON Zamowienia
    FOR EACH ROW
	BEGIN -- validate PESEL
	    IF new.kwota < 0 THEN
		SIGNAL SQLSTATE '45000'
		SET MESSAGE_TEXT= 'Kwota nie może być mniejsza od zera';
	    END IF;
	END;
	$$
DELIMITER ;








DELIMITER $$

CREATE OR REPLACE TRIGGER check_update_Zamowienia BEFORE UPDATE ON Zamowienia
    FOR EACH ROW
	BEGIN -- validate PESEL
	    IF new.kwota < 0 THEN
		SIGNAL SQLSTATE '45000'
		SET MESSAGE_TEXT= 'Kwota nie może być mniejsza od zera';
	    END IF;
	END;
	$$	
DELIMITER ;









DELIMITER $$

CREATE OR REPLACE TRIGGER check_insert_Telefon BEFORE INSERT ON Telefon
    FOR EACH ROW
	BEGIN -- validate PESEL
	    IF new.numer <100000000 OR new.numer>999999999 THEN
		SIGNAL SQLSTATE '45000'
		SET MESSAGE_TEXT= 'Podaj poprawny numer';
	    END IF;
	END;
	$$	
DELIMITER ;








DELIMITER $$

CREATE OR REPLACE TRIGGER check_update_Telefon BEFORE UPDATE ON Telefon
    FOR EACH ROW
	BEGIN -- validate PESEL
	    IF new.numer <100000000 OR new.numer>999999999 THEN
		SIGNAL SQLSTATE '45000'
		SET MESSAGE_TEXT= 'Podaj poprawny numer';
	    END IF;
	END;
	$$	
DELIMITER ;
