-- TODO: create a procedure for creating new users with specified privileges?

DROP USER IF EXISTS 'admin'@'%';
DROP USER IF EXISTS 'sprzedawca'@'%';
DROP USER IF EXISTS 'kierownik'@'%';
DROP USER IF EXISTS 'wlasciciel'@'%';
DROP USER IF EXISTS 'ksiegowa'@'%';

CREATE USER 'admin'@'localhost' IDENTIFIED BY 'admin';
CREATE USER 'admin'@'%' IDENTIFIED BY 'admin';
CREATE USER 'sprzedawca'@'localhost' IDENTIFIED BY 'sprzedawca';
CREATE USER 'sprzedawca'@'%' IDENTIFIED BY 'sprzedawca';
CREATE USER 'kierownik'@'localhost' IDENTIFIED BY 'kierownik';
CREATE USER 'kierownik'@'%' IDENTIFIED BY 'kierownik';
CREATE USER 'wlasciciel'@'localhost' IDENTIFIED BY 'wlasciciel';
CREATE USER 'wlasciciel'@'%' IDENTIFIED BY 'wlasciciel';
CREATE USER 'ksiegowa'@'localhost' IDENTIFIED BY 'ksiegowa';
CREATE USER 'ksiegowa'@'%' IDENTIFIED BY 'ksiegowa';

GRANT ALL ON flower_power.* TO 'admin'@'localhost';

GRANT SELECT ON flower_power.Produkty TO 'sprzedawca'@'localhost';
GRANT SELECT ON flower_power.Zamowienia TO 'sprzedawca'@'localhost';
GRANT SELECT ON flower_power.Zawartosc TO 'sprzedawca'@'localhost';
GRANT INSERT ON flower_power.Zamowienia TO 'sprzedawca'@'localhost';
GRANT INSERT ON flower_power.Zawartosc TO 'sprzedawca'@'localhost';


GRANT SELECT ON flower_power.Produkty TO 'kierownik'@'localhost';
GRANT SELECT ON flower_power.Zamowienia TO 'kierownik'@'localhost';
GRANT SELECT ON flower_power.Zawartosc TO 'kierownik'@'localhost';
GRANT INSERT ON flower_power.Zamowienia TO 'kierownik'@'localhost';
GRANT DELETE ON flower_power.Zamowienia TO 'kierownik'@'localhost';
GRANT INSERT ON flower_power.Zawartosc TO 'kierownik'@'localhost';
GRANT DELETE ON flower_power.Zawartosc TO 'kierownik'@'localhost';

GRANT INSERT ON flower_power.* TO 'wlasciciel'@'localhost';
GRANT SELECT ON flower_power.* TO 'wlasciciel'@'localhost';

GRANT SELECT ON flower_power.* TO 'ksiegowa'@'localhost';

GRANT ALL ON flower_power.* TO 'admin'@'%';

GRANT SELECT ON flower_power.Produkty TO 'sprzedawca'@'%';
GRANT SELECT ON flower_power.Zamowienia TO 'sprzedawca'@'%';
GRANT SELECT ON flower_power.Zawartosc TO 'sprzedawca'@'%';
GRANT INSERT ON flower_power.Zamowienia TO 'sprzedawca'@'%';
GRANT INSERT ON flower_power.Zawartosc TO 'sprzedawca'@'%';


GRANT SELECT ON flower_power.Produkty TO 'kierownik'@'%';
GRANT SELECT ON flower_power.Zamowienia TO 'kierownik'@'%';
GRANT SELECT ON flower_power.Zawartosc TO 'kierownik'@'%';
GRANT INSERT ON flower_power.Zamowienia TO 'kierownik'@'%';
GRANT DELETE ON flower_power.Zamowienia TO 'kierownik'@'%';
GRANT INSERT ON flower_power.Zawartosc TO 'kierownik'@'%';
GRANT DELETE ON flower_power.Zawartosc TO 'kierownik'@'%';

GRANT INSERT ON flower_power.* TO 'wlasciciel'@'%';
GRANT SELECT ON flower_power.* TO 'wlasciciel'@'%';

GRANT SELECT ON flower_power.* TO 'ksiegowa'@'%';
